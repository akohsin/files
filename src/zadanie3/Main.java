package zadanie3;
// Zadanie 3:
//        Stwórz aplikację, która służy do przeglądania katalogów.
//        Przyjmuj w pętli linię z wejścia. Po odczytaniu linii przekaż ją do obiektu typu File,
//          a następnie wypisz o nim kolejno informacje:
//
//        Plik/katalog istnieje: Tak/Nie
//        Ścieżka relatywna:
//        Ścieżka absolutna:
//        Wielkość pliku/katalogu:
//        Ścieżka absolutna:
//        Data ostatniej modyfikacji:
//        Czy plik jest ukryty:
//        Prawo dostępu do odczytu:
//        Prawo dostępu do zapisu:
//        Prawo dostępu do wykonywania(Executable):
//
//        (Jeśli jest katalogiem)
//        Lista plików w tym katalogu (wraz z ich datą modyfikacji oraz rozmiarem [oddzielone spacjami])
//

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        File pliczek = new File(sc.nextLine());
        if (pliczek.exists()) {
            System.out.println("plik istnieje");
            if (pliczek.isDirectory()) {
                System.out.println("pliczek jest katalogiem");
            }
            System.out.println("sciezka absolutna: " + pliczek.getAbsolutePath());
            System.out.println("sciezka relatywna: " + pliczek.getPath());

        } else System.out.println("plik nie istnieje");


    }

}
