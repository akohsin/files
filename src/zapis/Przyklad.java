package zapis;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Przyklad {
    public static void main(String[] args) {
        // BufferedReader < czytania
        // PrintWriter > zapis

        try (PrintWriter writer = new PrintWriter("plik.txt")) {
            writer.println("tekst\nawdawdawdawd");
            writer.flush(); // wyczyszczenie bufora i wysłanie zmian na dysk
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}