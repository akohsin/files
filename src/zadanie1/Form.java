package zadanie1;

public class Form {
    private String wiek ;
    private String plec ;
    private String nazwa ;
    private String odpowiedz1;
    private String odpowiedz2;
    private String odpowiedz3;

    public String getPlec() {
        return plec;
    }

    @Override
    public String toString() {
        return "Form" +
                "wiek: " + wiek + '\n'+  "plec: "  + plec + '\n' +
                " nazwa: " + nazwa + '\n' +
                " odpowiedz na pierwsze pytanie: " + odpowiedz1 + '\n' +
                " odpowiedz na drugie pytanie: " + odpowiedz2 + '\n' +
                " odpowiedz na trzecie pytanie: " + odpowiedz3 + '\n';

    }

    public void setWiek(String wiek) {
        this.wiek = wiek;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setOdpowiedz1(String odpowiedz1) {
        this.odpowiedz1 = odpowiedz1;
    }

    public void setOdpowiedz2(String odpowiedz2) {
        this.odpowiedz2 = odpowiedz2;
    }

    public void setOdpowiedz3(String odpowiedz3) {
        this.odpowiedz3 = odpowiedz3;
    }
}
