package zadanie1;
//Należy napisać aplikację (wykorzystując załączony kod) która w pętli while czyta ze Scannera wejście użytkownika z konsoli,
//        a następnie linia po linii wypisuje tekst do pliku.
//        Aplikacja ma się zamykać po wpisaniu przez użytkownika komendy "quit".

//          Zadanie 2:
//        Rozwijamy dotychczasową aplikację. Teraz - oczekujemy od użytkownika konkretnych danych,
//          które będą zapisywane do pliku. Aplikacja będzie służyć jako formularz który zbiera dane (jak z ankiety).
//          Zakładamy ankietę użytkownika która pyta użytkownika o następujące dane:
//
//        Podaj imie i nazwisko:
//        Podaj wiek:
//        Podaj płeć:
//
//        Kolejne pytania zadajemy dopiero wtedy, gdy użytkownik jest:
//        a) kobietą w wieku między 18-25
//        b) mężczyzną w wieku między 25-30
//
//        (powiedzmy że chodzi o jednakową dojrzałość osób biorących udział w ankiecie)
//        Wymyśl 3 pytania i zadaj je ankietowanemu.
//        Wszystkie pytania i odpowiedzi ankiety zapisz do pliku.
//        Otwieraj plik wyłącznie do dopisywania danych (jeśli istnieje).
//
//        Rozszerzenie:
//        Stwórz klasę modelu (Form) która przechowuje odpowiedzi na wszystkie pytania. Dodaj metodę toString,
//          która będzie używana do wypisania obiektu do pliku.
//        https://bitbucket.org/nordeagda2/simpleformapplication


import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String consoleLine;

        try (PrintWriter writer = new PrintWriter(new FileWriter("plik.txt", true))) {
            System.out.println("quit wychodzi, inny znak idzie dalej");

            while (true) {
                Form model = new Form();
                System.out.println("podaj imie i nazwisko");
                consoleLine = sc.nextLine();
                if (consoleLine.trim().equals("quit")) {
                    writer.flush();
                    break;
                }
                model.setNazwa(consoleLine);

                System.out.println("wiek");
                consoleLine = sc.nextLine();
                model.setWiek(consoleLine);
                int wiek = Integer.parseInt(consoleLine.trim());

                System.out.println("plec :");
                model.setPlec(sc.nextLine());

                if (model.getPlec().equals("mezczyzna") && wiek >= 25 && wiek <= 30 ||
                        model.getPlec().toLowerCase().equals("kobieta") && wiek >= 18 && wiek <= 25) {

                    System.out.println("ulubiony kolor?");
                    model.setOdpowiedz1(sc.nextLine());

                    System.out.println("Co było pierwsze jajo czy kura?");
                    model.setOdpowiedz2(sc.nextLine());

                    System.out.println("pytanie?");
                    model.setOdpowiedz3(sc.nextLine());

                }
                writer.print(model.toString());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }
}
